# User Interface Specification Document 

The outer container consist of **three inner containers**. 

1. First one is the top of the outer container and it is header. 
   - Header has 3 buttons;
     - First buttton is for adding a new user, 
     - Second radio button is for hide disabled user or show them,
     - Third button is for save user button.
2. Clicking on a new user button opens the right half container of the outer container and in that container there are text input fields to add new user's username, display name, phone, email, user roles button as choice box and radio button to show user is enabled or not and.   

3. On the left half container, there is a list to show added users with parameters(ID, USER NAME, EMAIL, ENABLED). List can be sorted and filtered for all parameters. 

Easy to use. 
